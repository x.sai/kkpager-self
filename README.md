# kkpager-self

#### 介绍
修改kkpager分页相关的逻辑（按淘宝分页）。

#### 软件架构
| 技术      | 版本      |
|---------|---------|
| jQuery  | v1.11.1 |
| kkpager | V1.3    |

![输入图片说明](https://images.gitee.com/uploads/images/2020/1112/145142_35d80db1_934851.png "效果图")
